#include "Roomba.h"
#include <iostream>

int main()
{
	MODEL model = _500;
	COMM commType = _UDP;
	int port = 7;
	string arduinoIP = "192.168.1.178";
	Roomba roomba(model, commType,
		port, arduinoIP);

	roomba.Startup();
	Sleep(100);
	//roomba.pwmMotor(-24, 0, 30);
	//roomba.Drive(1, 0);
	//Sleep(3000);
	//roomba.pwmMotor(0, 0, 0);
	Sleep(10);

	while (true)
	{
		roomba.getP1();
		cout << roomba.CliffFL << endl;
		Sleep(300);
	}

	system("pause");

	return 0;
}