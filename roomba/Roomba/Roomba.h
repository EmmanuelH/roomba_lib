/**	\file Roomba.h
Roomba class header file.
Contains the declarations for the Roomba class.

Author: Emmanuel Harley
Date: 18/12/2014
*/
#ifndef _ROOMBA_
#define _ROOMBA_

#include "Communication\Comm.h"
#include "Roomba\RoombaSensors.h"
//TODO: CONSTRUCT ROOMBA SENSOR CLASS

/// Main Roomba class to control a roomba
class Roomba : public RoombaSensors
{
public:
	Roomba(MODEL roombaModel, COMM commType, int port, string destIP = "255.255.255.255");
	~Roomba();

	void Startup();
	void Drive(float LvelocityInputMagnitude, float RturnInputMagnitude);
	void Stop();
	void ReadInput();

	void pwmMotor(_int8 mainBrush, _int8 sideBrush, _int8 vacuum);

	MODEL getModel() { return model; }
private:
	MODEL model;
};

#endif //_ROOMBA_