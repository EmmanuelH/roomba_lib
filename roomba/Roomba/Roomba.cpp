/**	\file Roomba.cpp
Roomba class source file.
Contains the definitions for the Roomba class and its members.

Author: Emmanuel Harley
Date: 18/12/2014
*/
#include "Roomba.h"

/** Roomba Constructor
	
	\param roombaModel Model number of roomba ex: _500
	\param commType Communication type being used ex: _UDP
	\param port Port being used for serial or UDP port
	\param destIP The destination IP of the Roomba, "" by default
*/
Roomba::Roomba(MODEL roombaModel, COMM commType, int port, string destIP)
{
	model = roombaModel;
	long baud;
	if (model == _400)
		baud = CBR_57600;
	else if (model == _500)
		baud = CBR_115200;

	comm.Setup(commType, port, baud, destIP);

	model = roombaModel;
}

/**	Roomba destructor
	
	\todo actually destruct...
*/
Roomba::~Roomba()
{
}

/** Sends Startup codes to Roomba
	
	Startup codes are different depending on the roomba.
	The 400 series is older and does not support all the functionality the
	500 series supports. Mainly due to a lack of sensors.
*/
void Roomba::Startup()
{
	_int8 startup[3] = { 128, 130, 131 };
	switch (model){
	case _400:
		comm.Send(startup, 3);
		break;

	case _500:
		comm.Send(startup, 2);
		break;

	default:
		break;
	}
}

/** Sends Drive Commands to Roomba

	\param LvelocityInputMagnitude The magnitude of the velocity as a percentage range of -1.0 to 1.0 for the left wheel
	\param RvelocityInputMagnitude The magnitude of the velocity as a percentage range of -1.0 to 1.0 for the right wheel
*/
void Roomba::Drive(float LvelocityInputMagnitude, float RturnInputMagnitude)
{
	_int8 drive[5] = { 0 };

	switch (model){
	case _400:
		drive[0] = 137;
		break;

	case _500:
		drive[0] = 137;
		break;

	default:
		break;
	}

	//assigns magnitudes for each motor.
	_int16 LvelocityMagnitude = 32767 * LvelocityInputMagnitude;
	_int16 RturnMagnitude = RturnInputMagnitude * 32767;
	
	// 0 means go straight:
	if (RturnMagnitude == 0)
		RturnMagnitude = 0x8000;
	
	//set velocity - big endian

	//get highbyte
	drive[1] = LvelocityMagnitude >> 8;
	//get the low byte
	drive[2] = LvelocityMagnitude & 0xFF;

	// get highbyte
	drive[3] = RturnMagnitude >> 8;
	// get the low byte
	drive[4] = RturnMagnitude & 0xFF;

	// sends drive command
	comm.Send(drive, 5);
}

/** Sends a pwm signal to the main and side brush. As well as the motor
	
	\param mainBrush The PWM value to send to the main brush (range: -127-127)
	\param sideBrush The PWM value to send to the main brush (range: -127-127)
	\param vacuum The PWM value to send to the vacuum (range: 0-127)
*/
void Roomba::pwmMotor(_int8 mainBrush, _int8 sideBrush, _int8 vacuum)
{
	_int8 motor[4] = { 144, mainBrush, sideBrush, vacuum };
	comm.Send(motor, 4);
}
