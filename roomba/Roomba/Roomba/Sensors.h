#ifndef SENSORS_H
#define SENSORS_H

struct Sensors{
	// Bumps and wheel drops packet ID: 7
	bool bRight = false;	/// button 1 Packet ID: 7
	bool bLeft = false;		/// button 2 Packet ID: 7
	bool wdRight = false;	/// button 4 Packet ID: 7
	bool wdLeft = false;	/// button 8 Packet ID: 7

	//packet ID's: 8-13
	bool Wall = false;		/// Wall Packet ID: 8
	bool CliffL = false;	/// Cliff Left Packet ID: 9
	bool CliffFL = false;	/// Cliff Front Left Packet ID: 10
	bool CliffFR = false;	/// Cliff Front Right Packet ID: 11
	bool CliffR = false;	/// Cliff Right Packet ID: 12
	bool vWall = false;		/// Virtual Wall Packet ID: 13

	//wheel overcurrents packet ID: 14
	bool SideBrush = false;	/// button 1 Packet ID: 14
	bool MainBrush = false;	/// button 2 Packet ID: 14
	bool RightWheel = false;/// button 4 Packet ID: 14
	bool LeftWheel = false;	/// button 8 Packet ID: 14

	/// dirt level Packet ID:15
	unsigned _int8 dirtLevel = 0;

	/// Infrared Character Omni packet ID: 17
	_int8 irCharOmni = 0;

	//packet ID: 18
	bool Clean = false;		/// button 1 Packet ID: 18
	bool Spot = false;		/// button 2 Packet ID: 18
	bool Dock = false;		/// button 4 Packet ID: 18
	bool Minute = false;	/// button 8 Packet ID: 18
	bool Hour = false;		/// button 16 Packet ID: 18
	bool Day = false;		/// button 32 Packet ID: 18
	bool Schedule = false;	/// button 64 Packet ID: 18
	bool Clock = false;		/// button 128 Packet ID: 18

	/// Distance packet ID: 19
	_int16 Distance = 0;

	/// Angle packet id: 20
	_int16 Angle = 0;

	/// charging state packet id: 21
	_int8 ChargingState = 0;

	/// voltage packet id: 22
	unsigned _int16 Voltage = 0;

	/// Current packet id: 23
	_int16 Current = 0;

	/// Temperature packet id: 24
	_int8 Temp = 0;

	/// battery charge packet id: 25
	unsigned _int16 BattCharge = 0;
	
	/// battery capacity packet id: 26	
	unsigned _int16 BattCap = 0;

	/// Wall Signal packet id: 27
	unsigned _int8 WallSignal = 0;

	/// Cliff Left Signal Package ID: 28
	unsigned _int8 CliffLSignal = 0;

	/// Cliff Front Left Signal Package ID: 29
	unsigned _int8 CliffFLSignal = 0;

	/// Cliff Front Right Signal Package ID : 30
	unsigned _int8 CliffFRSignal = 0;

	/// Cliff Right Signal Package ID: 31
	unsigned _int8 CliffRSignal = 0;

	/// Charging Sources Available Packet ID : 34
	unsigned _int8 ChargingSources = 0;

	/// OI Mode Packet ID: 35
	unsigned _int8 OIMode = 0;

	/// Song Number Packet ID: 36
	unsigned _int8 SongNumber = 0;

	/// Song Playing Packet ID: 37
	bool SongPlaying = false;

	/// Number of Stream Packets Packet ID: 38
	unsigned _int8 StreamPacketCount = 0;

	/// Requested Velocity Packet ID: 39
	_int16 LastDrive = 0;

	/// Requested Radius Packet ID: 40
	_int16 LastRad = 0;

	/// Requested Right Velocity Packet ID: 41
	_int16 LastDriveRight = 0;

	/// Requested Left Velocity Packet ID: 42
	_int16 LastDriveLeft = 0;

	/// Right Encoder Counts Packet ID: 43
	unsigned _int16 EncoderRightCount = 0;

	/// Left Encoder Counts Packet ID: 44
	unsigned _int16 EncoderLeftCount = 0;

	/// Light Bumper Package ID: 45
	unsigned _int8 LightBumps = 0;

	/// Light Bump Left Signal Package ID: 46
	unsigned _int16 LightBumpLSignal = 0;

	/// Light Bump Front Left Signal Package ID: 47
	unsigned _int16 LightBumpFLSignal = 0;

	/// Light Bump Center Left Signal Package ID: 48
	unsigned _int16 LightBumpCLSignal = 0;

	/// Light Bump Center Right Signal Package ID: 49
	unsigned _int16 LightBumpCRSignal = 0;

	/// Light Bump Front Right Signal Package ID: 50
	unsigned _int16 LightBumpFRSignal = 0;

	/// Light Bump Right Signal Package ID: 51
	unsigned _int16 LightBumpRSignal = 0;

	/// Infrared Character Left Packet ID: 52
	unsigned _int8 IRCharacterLeft = 0;

	/// Infrared Character Right Packet ID: 53
	unsigned _int8 IRCharacterRight = 0;

	/// Left Motor Current Packet ID: 54
	_int16 MotorLeftCurrent = 0;

	/// Right Motor Current Packet ID: 55
	_int16 MotorRightCurrent = 0;

	/// Main Brush Motor Current Packet ID: 56
	_int16 MotorMainBrushCurrent = 0;

	/// Side Brush Motor Current Packet ID: 57
	_int16 MotorSideBrushCurrent = 0;

	/// Stasis Packet ID: 58
	bool Stasis = false;
};

#endif //SENSORS_H