/**	\file RoombaSensors.h
RoombaSensors Class....

Author: Emmanuel Harley
Date: 06/01/2015
*/
#ifndef ROOMBASENSORS_H
#define ROOMBASENSORS_H

#include "..\Communication\Comm.h"
#include "RoombaConstants.h"
#include "Sensors.h"

struct RoombaSensors : public Sensors
{
	Comm comm;

	int getMotorOvercurrents(int);
	int getButtonState(int);

	void getSensor(_int8 sensor, _int8 song);

	// group packet id 0
	void getP0();
	void printP0();

	// group packet id 1
	void getP1();
	void printP1();

	// group packet id 2
	void getP2();
	void printP2();

	// group packet id 3
	void getP3();
	void printP3();

	// group packet id 4
	void getP4();
	void printP4();

	// group packet id 5
	void getP5();

	// group packet id 101
	void getP101();

protected:
	_int16 charToShort(unsigned _int8 lowByte, unsigned _int8 highByte);
};

#endif //_ROOMBASENSORS_