/**	\file RoombaConstants.h
Defines constants for roomba sensors and motors.

Author: Emmanuel Harley
Date: 06/01/2015
*/

#ifndef _Roomba_Constants_
#define _Roomba_Constants_

/// Roomba model types
enum MODEL{
	_400, _500
};

//Group Packets

/// Group Packet ID 0
#define ROOMBA_GID_0 0
/// Group Packet ID 0 bit
#define ROOMBA_GID_0_BIT 1
/// Group Packet ID 0 size
#define ROOMBA_GID_0_SIZE 26

/// Group Packet ID 1
#define ROOMBA_GID_1 0
/// Group Packet ID 1 bit
#define ROOMBA_GID_1_BIT 2
/// Group Packet ID 1 size
#define ROOMBA_GID_1_SIZE 10

/// Group Packet ID 2
#define ROOMBA_GID_2 2
/// Group Packet ID 2 bit
#define ROOMBA_GID_2_BIT 4
/// Group Packet ID 2 size
#define ROOMBA_GID_2_SIZE 6

/// Group Packet ID 3
#define ROOMBA_GID_3 3
/// Group Packet ID 3 bit
#define ROOMBA_GID_3_BIT 8
/// Group Packet ID 3 size
#define ROOMBA_GID_3_SIZE 10

/// Group Packet ID 4
#define ROOMBA_GID_4 4
/// Group Packet ID 4 bit
#define ROOMBA_GID_4_BIT 16
/// Group Packet ID 4 size
#define ROOMBA_GID_4_SIZE 14

/// Group Packet ID 5
#define ROOMBA_GID_5 5
/// Group Packet ID 5 bit
#define ROOMBA_GID_5_BIT 32
/// Group Packet ID 5 size
#define ROOMBA_GID_5_SIZE 12

/// Group Packet ID 101
#define ROOMBA_GID_101 101
/// Group Packet ID 101 bit
#define ROOMBA_GID_101_BIT 64
/// Group Packet ID 101 size
#define ROOMBA_GID_101_SIZE 28


// ROOMBA MOTOR CONSTANTS
#define ROOMBA_MOTOR_MAIN_BRUSH 4;
#define ROOMBA_MOTOR_VACUUM 2;
#define ROOMBA_MOTOR_SIDE_BRUSH  1;

/// Bumps and Wheel Drops
#define ROOMBA_PID_7 7
/// Bumps and Wheel Drops Size
#define ROOMBA_PID_7_SIZE 1

// BUMP WHEEL DROP CONSTANTS
#define ROOMBA_WHEELDROP_LEFT 8;	/// Left Wheel Drop bit
#define ROOMBA_WHEELDROP_RIGHT 4;	/// Right Wheel Drop bit
#define ROOMBA_BUMP_LEFT 2;		/// Left Bump bit
#define ROOMBA_BUMP_RIGHT 1;		/// Right Bump bit

/// Wall
#define ROOMBA_PID_8 8
/// Wall Bit
#define ROOMBA_PID_8_BIT 1
/// Wall Size
#define ROOMBA_PID_8_SIZE 1

/// Cliff Left
#define ROOMBA_PID_9 9
/// Cliff Left Size
#define ROOMBA_PID_9_SIZE 1

/// Cliff Front Left
#define ROOMBA_PID_10 10
/// Cliff Front Left Size
#define ROOMBA_PID_10_SIZE 1

/// Cliff Front Right
#define ROOMBA_PID_11 11
/// Cliff Front Right Size
#define ROOMBA_PID_11_SIZE 1

/// Cliff Right
#define ROOMBA_PID_12 12
/// Cliff Right Size
#define ROOMBA_PID_12_SIZE 1

/// Virtual Wall
#define ROOMBA_PID_13 13
/// Virtual Wall Size
#define ROOMBA_PID_13_SIZE 1

/// Wheel Overcurrents
#define ROOMBA_PID_14 14
/// Wheel Overcurrents Size
#define ROOMBA_PID_14_SIZE 1

// MOTOR OVERCURRENT CONSTANTS
#define ROOMBA_OVERCURRENT_DRIVE_LEFT_WHEEL 16;	/// Left wheel overcurrent bit
#define ROOMBA_OVERCURRENT_DRIVE_RIGHT_WHEEL 8;	/// Right wheel overcurrent bit
#define ROOMBA_OVERCURRENT_MAIN_BRUSH 4;			/// Main brush overcurrent bit
#define ROOMBA_OVERCURRENT_SIDE_BRUSH 1;			/// Side brush overcurrent bit

// Dirt Detect
#define ROOMBA_PID_15 15

// BUTTONS CONSTANTS
#define ROOMBA_BUTTON_POWER 8;
#define ROOMBA_BUTTON_SPOT 4;
#define ROOMBA_BUTTON_CLEAN 2;
#define ROOMBA_BUTTON_MAX 1;

// CHARGING STATES CONSTANTS
#define ROOMBA_STATE_NOT_CHARGING 0;
#define ROOMBA_STATE_CHARGING_RECOVERY 1;
#define ROOMBA_STATE_CHARGING 2;
#define ROOMBA_STATE_TRICKLE_CHARGING 3;
#define ROOMBA_STATE_WAITING 4;
#define ROOMBA_STATE_CHARGING_ERROR 5;

/// \endsecreflist

#endif //_Roomba_Constants_