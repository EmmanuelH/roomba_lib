#include "RoombaSensors.h"



//Returns true if the wheel is dropped or on a bump
//Valid input for field: CASTER, LEFT, RIGHT
//Valid input for sensor:BUMP, WHEELDROP
/*
int RoombaSensors::getBumpWD(int field, SensorFields sensor)
{
	if (sensor == BUMP) {
		switch (field) {
		case ROOMBA_BUMP_RIGHT:
			return ((BumpWD & ROOMBA_BUMP_RIGHT) == ROOMBA_BUMP_RIGHT)
				? true : false;
			break;

		case ROOMBA_BUMP_LEFT:
			return ((BumpWD & ROOMBA_BUMP_LEFT) == ROOMBA_BUMP_LEFT)
				? true : false;
			break;
		}
	}
	else if (sensor == WHEELDROP) {
		switch (field) {
		case ROOMBA_WHEELDROP_CASTER:
			return ((BumpWD & ROOMBA_WHEELDROP_CASTER) == ROOMBA_WHEELDROP_CASTER)
				? true : false;
			break;

		case ROOMBA_WHEELDROP_LEFT:
			return ((BumpWD & ROOMBA_WHEELDROP_LEFT) == ROOMBA_WHEELDROP_LEFT)
				? true : false;
			break;

		case ROOMBA_WHEELDROP_RIGHT:
			return ((BumpWD & ROOMBA_WHEELDROP_RIGHT) == ROOMBA_WHEELDROP_RIGHT)
				? true : false;
			break;
		}
	}

	//something went wrong...
	return -1;
}
*/

//returns the state of the selected overcurrent sensor
int RoombaSensors::getMotorOvercurrents(int motor)
{
	/*
	switch (motor) {
	case ROOMBA_OVERCURRENT_DRIVE_LEFT_MOTOR:
		return ((MotorOvercurrents & ROOMBA_OVERCURRENT_DRIVE_LEFT_MOTOR) == ROOMBA_OVERCURRENT_DRIVE_LEFT_MOTOR)
			? true : false;
		break;

	case ROOMBA_OVERCURRENT_DRIVE_RIGHT_MOTOR:
		return ((MotorOvercurrents & ROOMBA_OVERCURRENT_DRIVE_RIGHT_MOTOR) == ROOMBA_OVERCURRENT_DRIVE_RIGHT_MOTOR)
			? true : false;
		break;

	case ROOMBA_OVERCURRENT_MAIN_BRUSH:
		return ((MotorOvercurrents & ROOMBA_OVERCURRENT_MAIN_BRUSH) == ROOMBA_OVERCURRENT_MAIN_BRUSH)
			? true : false;
		break;

	case ROOMBA_OVERCURRENT_VACUUM:
		return ((MotorOvercurrents & ROOMBA_OVERCURRENT_VACUUM) == ROOMBA_OVERCURRENT_VACUUM)
			? true : false;
		break;

	case ROOMBA_OVERCURRENT_SIDE_BRUSH:
		return ((MotorOvercurrents & ROOMBA_OVERCURRENT_SIDE_BRUSH) == ROOMBA_OVERCURRENT_SIDE_BRUSH)
			? true : false;
		break;
	}
	*/
	//something went wrong
	return -1;
}

//get selected button state
int RoombaSensors::getButtonState(int button)
{
	/*
	switch (button) {
	case ROOMBA_BUTTON_POWER:
		return ((MotorOvercurrents & ROOMBA_BUTTON_POWER) == ROOMBA_BUTTON_POWER)
			? true : false;
		break;

	case ROOMBA_BUTTON_SPOT:
		return ((MotorOvercurrents & ROOMBA_BUTTON_SPOT) == ROOMBA_BUTTON_SPOT)
			? true : false;
		break;

	case ROOMBA_BUTTON_CLEAN:
		return ((MotorOvercurrents & ROOMBA_BUTTON_CLEAN) == ROOMBA_BUTTON_CLEAN)
			? true : false;
		break;

	case ROOMBA_BUTTON_MAX:
		return ((MotorOvercurrents & ROOMBA_BUTTON_MAX) == ROOMBA_BUTTON_MAX)
			? true : false;
		break;
	}
	*/
	//something went wrong
	return -1;
}

void RoombaSensors::getSensor(_int8 sensor, _int8 song)
{
	_int8 request[3] = { 142, sensor, song };

	// send request
	comm.Send(request, 2);
	Sleep(10);
}

/** Gets the group packet ID 0
	
	This group packet ID consists of packet id's 7-26
*/
void RoombaSensors::getP0()
{
	_int8 data[ROOMBA_GID_0_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_0 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_0_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_0_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_0_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// packet ID: 7
	if ((data[0] & 1) == 1)
		bRight = true;
	if ((data[0] & 2) == 2)
		bLeft = true;
	if ((data[0] & 4) == 4)
		wdRight = true;
	if ((data[0] & 8) == 8)
		wdLeft = true;

	//packet ID's: 8-13
	Wall = data[1];
	CliffL = data[2]; 
	CliffFL = data[3]; 
	CliffFR = data[4];
	CliffR = data[5];
	vWall = data[6];
	
	// wheel overcurrents packet ID : 14
	if ((data[7] & 1) == 1)
		SideBrush = true;
	if ((data[7] & 4) == 4)
		MainBrush = true;
	if ((data[7] & 8) == 8)
		RightWheel = true;
	if ((data[7] & 16) == 16)
		LeftWheel = true;

	//dirt level Packet ID:15
	dirtLevel = data[8];

	// unsused Packet ID: 16

	// Infrared Character Omni packet ID: 17
	irCharOmni = data[10];

	// button Packet ID: 18
	if ((data[11] & 1) == 1)
		Clean = true;//spot
	if ((data[11] & 2) == 2)
		Spot = true;//day
	if ((data[11] & 4) == 4)
		Dock = true;
	if ((data[11] & 8) == 8)
		Minute = true;
	if ((data[11] & 16) == 16)
		Hour = true; //same thing as day
	if ((data[11] & 32) == 32)
		Day = true; //cchanges battcap and current but not days state
	if ((data[11] & 64) == 64)
		Schedule = true; // does nothing
	if ((data[11] & 128) == 128)
		Clock = true; // does nothing

	// distance packet ID: 19
	/// \todo high byte first fix
	Distance = charToShort(data[13], data[12]);

	// angle packet id: 20
	/// \todo hight byte first fix
	Angle = charToShort(data[15], data[14]);

	// charging state packet id: 21
	ChargingState = data[16];

	// voltage packet id: 22
	/// \todo test...
	Voltage = charToShort(data[18], data[17]);

	// current packet id: 23
	Current = charToShort(data[20], data[19]);

	// temperature packet id: 24
	Temp = data[21];

	// battery charge packet id: 25
	BattCharge = charToShort(data[23], data[22]);

	// battery capacity packet id: 26	
	BattCap = charToShort(data[25], data[24]);
}

/** Prints all values of Group Packet ID 0

	This group packet ID consists of packet id's 7-26
*/
void RoombaSensors::printP0()
{
	cout << "Bump R: " << bRight << "\t\tBump L: " << bLeft << endl;
	cout << "WD R: " << wdRight << "\t\tWD L: " << wdLeft << endl;
	cout << "Wall: " << Wall << "\t\tV Wall: " << vWall << endl;
	cout << "Cliff L: " << CliffL << "\t\tCliff FL: " << CliffFL << endl;
	cout << "Cliff FR: " << CliffFR << "\t\tCliff R: " << CliffR << endl;
	cout << "sBrush: " << SideBrush << "\t\tmBrush: " << MainBrush << endl;
	cout << "rWheel: " << RightWheel << "\t\tlWheel: " << LeftWheel << endl;
	cout << "dirt Level: " << int(dirtLevel) << endl;

	cout << "irCharOmni: " << int(irCharOmni) << "\t\tClean: " << Clean << endl;
	cout << "Spot: " << Spot << "\t\tDock: " << Dock << endl;
	cout << "Minute: " << Minute << "\t\tHour: " << Hour << endl;
	cout << "Day: " << Day << "\t\tSchedule: " << Schedule << endl;
	cout << "Clock: " << Clock << "\t\tDistance: " << Distance;

	cout << "Angle: " << Angle << "\tCharging State: " << int(ChargingState) << endl;
	cout << "Voltage: " << Voltage << "\t\tCurrent: " << Current << endl;
	cout << "Temp: " << int(Temp) << "\t\tBattCharge: " << BattCharge << endl;
	cout << "battCap: " << BattCap << endl;
}

/** Gets the group packet ID 1

	This group packet consists of packet id's 7-16
*/
void RoombaSensors::getP1()
{
	_int8 data[ROOMBA_GID_1_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_1 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_1_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_1_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_1_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// packet ID: 7
	if ((data[0] & 1) == 1)
		bRight = true;
	if ((data[0] & 2) == 2)
		bLeft = true;
	if ((data[0] & 4) == 4)
		wdRight = true;
	if ((data[0] & 8) == 8)
		wdLeft = true;

	//packet ID's: 8-13
	Wall = data[1];
	CliffL = data[2];
	CliffFL = data[3];
	CliffFR = data[4];
	CliffR = data[5];
	vWall = data[6];

	// wheel overcurrents packet ID : 14
	if ((data[7] & 1) == 1)
		SideBrush = true;
	if ((data[7] & 4) == 4)
		MainBrush = true;
	if ((data[7] & 8) == 8)
		RightWheel = true;
	if ((data[7] & 16) == 16)
		LeftWheel = true;

	//dirt level Packet ID:15
	dirtLevel = data[8];

	// unsused Packet ID: 16
}

/** Prints all values of Group Packet ID 1

	This group packet consists of packet id's 7-16
*/
void RoombaSensors::printP1()
{
	cout << "Bump R: " << bRight << "\t\tBump L: " << bLeft << endl;
	cout << "WD R: " << wdRight << "\t\tWD L: " << wdLeft << endl;
	cout << "Wall: " << Wall << "\t\tV Wall: " << vWall << endl;
	cout << "Cliff L: " << CliffL << "\t\tCliff FL: " << CliffFL << endl;
	cout << "Cliff FR: " << CliffFR << "\t\tCliff R: " << CliffR << endl;
	cout << "sBrush: " << SideBrush << "\t\tmBrush: " << MainBrush << endl;
	cout << "rWheel: " << RightWheel << "\t\tlWheel: " << LeftWheel << endl;
	cout << "dirt Level: " << int(dirtLevel) << endl;
}

/** Gets the group packet ID 2

	This group packet consists of packet id's 17-20
*/
void RoombaSensors::getP2()
{
	_int8 data[ROOMBA_GID_2_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_2 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_2_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_2_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_2_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// Infrared Character Omni packet ID: 17
	irCharOmni = data[0];

	// button Packet ID: 18
	if ((data[1] & 1) == 1)
		Clean = true;//spot
	if ((data[1] & 2) == 2)
		Spot = true;//day
	if ((data[1] & 4) == 4)
		Dock = true;
	if ((data[1] & 8) == 8)
		Minute = true;
	if ((data[1] & 16) == 16)
		Hour = true; //same thing as day
	if ((data[1] & 32) == 32)
		Day = true; //cchanges battcap and current but not days state
	if ((data[1] & 64) == 64)
		Schedule = true; // does nothing
	if ((data[1] & 128) == 128)
		Clock = true; // does nothing

	// distance packet ID: 19
	/// \todo high byte first fix
	Distance = charToShort(data[3], data[2]);

	// angle packet id: 20
	/// \todo hight byte first fix
	Angle = charToShort(data[5], data[4]);
}

/** Prints all values of Group Packet ID 2

	This group packet consists of packet id's 17-20
*/
void RoombaSensors::printP2()
{
	cout << "irCharOmni: " << int(irCharOmni) << "\t\tClean: " << Clean << endl;
	cout << "Spot: " << Spot << "\t\tDock: " << Dock << endl;
	cout << "Minute: " << Minute << "\t\tHour: " << Hour << endl;
	cout << "Day: " << Day << "\t\tSchedule: " << Schedule << endl;
	cout << "Clock: " << Clock << "\t\tDistance: " << Distance;
	cout << "Angle: " << Angle << endl;
}

/** Gets the group packet ID 3

	This group packet consists of packet id's 21-26
*/
void RoombaSensors::getP3()
{
	_int8 data[ROOMBA_GID_3_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_3 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_3_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_3_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_3_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// charging state packet id: 21
	ChargingState = data[0];

	// voltage packet id: 22
	/// \todo test...
	Voltage = charToShort(data[2], data[1]);

	// current packet id: 23
	Current = charToShort(data[4], data[3]);

	// temperature packet id: 24
	Temp = data[5];

	// battery charge packet id: 25
	BattCharge = charToShort(data[7], data[6]);

	// battery capacity packet id: 26	
	BattCap = charToShort(data[9], data[8]);
}

/** Prints all values of Group Packet ID 3
	
	This group packet consists of packet id's 21-26
*/
void RoombaSensors::printP3()
{
	cout << int(ChargingState) << endl;
	cout << "Voltage: " << Voltage << "\t\tCurrent: " << Current << endl;
	cout << "Temp: " << int(Temp) << "\t\tBattCharge: " << BattCharge << endl;
	cout << "battCap: " << BattCap << endl;
}

/** Gets the group packet ID 4

	This group packet consists of packet id's 27-34
*/
void RoombaSensors::getP4()
{
	_int8 data[ROOMBA_GID_4_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_4 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_4_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_4_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_4_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// Wall Signal packet id: 27
	WallSignal = charToShort(data[1], data[0]);

	// Cliff Left Signal Package ID: 28
	CliffLSignal = charToShort(data[3], data[2]);

	// Cliff Front Left Signal Package ID: 29
	CliffFLSignal = charToShort(data[5], data[4]);

	// Cliff Front Right Signal Package ID: 30
	CliffFRSignal = charToShort(data[7], data[6]);

	// Cliff Right Signal Package ID: 31
	CliffRSignal = charToShort(data[9], data[8]);

	// Unused Packet ID: 32-33
	// 10 11 12


	// Charging Sources Available Packet ID: 34
	ChargingSources = data[13];
}

/** Prints all values of Group Packet ID 4

	This group packet consists of packet id's 27-34
*/
void RoombaSensors::printP4()
{
}

/** Gets the group packet ID 5

This group packet consists of packet id's 35-42
*/
void RoombaSensors::getP5()
{
	_int8 data[ROOMBA_GID_5_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_5 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_5_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_5_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_5_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// OI Mode Packet ID: 35
	OIMode = data[0];

	// Song Number Packet ID: 36
	SongNumber = data[1];

	// Song Playing Packet ID: 37
	SongPlaying = bool(data[2]);

	// Number of Stream Packets Packet ID: 38
	StreamPacketCount = data[3];

	// Requested Velocity Packet ID: 39
	LastDrive = charToShort(data[5], data[4]);

	// Requested Radius Packet ID: 40
	LastRad = charToShort(data[7], data[6]);

	// Requested Right Velocity Packet ID: 41
	LastDriveRight = charToShort(data[9], data[8]);

	// Requested Left Velocity Packet ID: 42
	LastDriveLeft = charToShort(data[11], data[10]);
}

/** Gets the group packet ID 101

This group packet consists of packet id's 43-58
*/
void RoombaSensors::getP101()
{
	_int8 data[ROOMBA_GID_101_SIZE], buff[100];
	_int8 request[2] = { 142, ROOMBA_GID_101 };

	// send request
	comm.Send(request, 2);

	// read data until we get full packet
	int byteCount = 0, index = 0;
	while (index + 1 < ROOMBA_GID_101_SIZE)
	{
		byteCount = comm.Read(buff, ROOMBA_GID_101_SIZE);
		for (int i = 0; i < byteCount && index + 1 < ROOMBA_GID_101_SIZE; index++, i++)
			data[index] = buff[i];
	}

	// Right Encoder Counts Packet ID : 43
	EncoderRightCount = charToShort(data[1], data[0]);

	// Left Encoder Counts Packet ID: 44
	EncoderLeftCount = charToShort(data[3], data[2]);

	// Light Bumper Package ID: 45
	LightBumps = data[4];

	// Light Bump Left Signal Package ID: 46
	LightBumpLSignal = charToShort(data[6], data[5]);

	// Light Bump Front Left Signal Package ID: 47
	LightBumpFLSignal = charToShort(data[8], data[7]);

	// Light Bump Center Left Signal Package ID: 48
	LightBumpCLSignal = charToShort(data[10], data[9]);

	// Light Bump Center Right Signal Package ID: 49
	LightBumpCRSignal = charToShort(data[12], data[11]);

	// Light Bump Front Right Signal Package ID: 50
	LightBumpFRSignal = charToShort(data[14], data[13]);

	// Light Bump Right Signal Package ID: 51
	LightBumpRSignal = charToShort(data[16], data[15]);

	// Infrared Character Left Packet ID: 52
	IRCharacterLeft = data[17];

	// Infrared Character Right Packet ID: 53
	IRCharacterRight = data[18];

	// Left Motor Current Packet ID: 54
	MotorLeftCurrent = charToShort(data[20], data[19]);

	// Right Motor Current Packet ID: 55
	MotorRightCurrent = charToShort(data[22], data[21]);

	// Main Brush Motor Current Packet ID: 56
	MotorMainBrushCurrent = charToShort(data[24], data[23]);

	// Side Brush Motor Current Packet ID: 57
	MotorSideBrushCurrent = charToShort(data[26], data[25]);

	// Stasis Packet ID: 58
	Stasis = data[27];
}

/** Converts a character into a short (two byte value)

	\param lowByte The low byte of the two byte integer
	\param highByte The high byte of the two byte integer

	\return Returns the two byte short.
*/
_int16 RoombaSensors::charToShort(unsigned _int8 lowByte, unsigned _int8 highByte)
{
	_int16 mine = highByte << 8;
	_int16 wow = lowByte;

	return mine | wow;
}