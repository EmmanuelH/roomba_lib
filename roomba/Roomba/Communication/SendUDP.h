#ifndef __SENDUDP_H__
#define __SENDUDP_H__

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <system_error>
#include <string>

#pragma once

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")
class WSASession
{
public:
	WSASession()
	{
		int ret = WSAStartup(MAKEWORD(2, 2), &data);
		if (ret != 0)
			printf("Server: Error at socket(): %ld\n", WSAGetLastError());
	}
	~WSASession()
	{
		WSACleanup();
	}
private:
	WSAData data;
};

class UDPSocket
{
public:
	UDPSocket()
	{
		sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (sock == INVALID_SOCKET)
			printf("Server: Error at socket(): %ld\n", WSAGetLastError());
	}
	~UDPSocket()
	{
		closesocket(sock);
	}

	void SendTo(const std::string& address, unsigned short port, const char* buffer, int len, int flags = 0)
	{
		sockaddr_in add;
		add.sin_family = AF_INET;
		add.sin_addr.s_addr = inet_addr(address.c_str());
		add.sin_port = htons(port);
		int ret = sendto(sock, buffer, len, flags, reinterpret_cast<SOCKADDR *>(&add), sizeof(add));
		if (ret < 0)
			printf("Server: Error at socket(): %ld\n", WSAGetLastError());
	}
	void SendTo(sockaddr_in& address, const char* buffer, int len, int flags = 0)
	{
		int ret = sendto(sock, buffer, len, flags, reinterpret_cast<SOCKADDR *>(&address), sizeof(address));
		if (ret < 0)
			printf("Server: Error at socket(): %ld\n", WSAGetLastError());
	}

private:
	SOCKET sock;
};

#endif //__SENDUDP_H__