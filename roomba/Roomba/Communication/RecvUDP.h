#ifndef __RecvUDP_H__
#define __RecvUDP_H__

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <system_error>
#include <string>
#include <iostream>

#pragma once

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")
class RecvUDP
{
public:
	RecvUDP()
	{
	}
	RecvUDP(int Port)
	{
		BufLength = 1024;
		SenderAddrSize = sizeof(SenderAddr);

		// Initialize Winsock version 2.2
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			printf("Server: WSAStartup failed with error %ld\n", WSAGetLastError());
		}

		// Create a new socket to receive datagrams on.
		ReceivingSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

		if (ReceivingSocket == INVALID_SOCKET)
		{
			printf("Server: Error at socket(): %ld\n", WSAGetLastError());
			// Clean up
			WSACleanup();
		}

		// Set up a SOCKADDR_IN structure that will tell bind that we
		// want to receive datagrams from all interfaces using port 5150.

		// The IPv4 family
		ReceiverAddr.sin_family = AF_INET;
		// Port no. 5150
		ReceiverAddr.sin_port = htons(Port);
		// From all interface (0.0.0.0)
		ReceiverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

		// Associate the address information with the socket using bind.
		// At this point you can receive datagrams on your bound socket.
		if (bind(ReceivingSocket, (SOCKADDR *)&ReceiverAddr, sizeof(ReceiverAddr)) == SOCKET_ERROR)
		{
			printf("Server: bind() failed! Error: %ld.\n", WSAGetLastError());
			// Close the socket
			closesocket(ReceivingSocket);
			// Do the clean up
			WSACleanup();
			// and exit with error
			printf("ERRRRRRRRORRRRRRRR\n\n\n\n\n");
		}

		// Some info on the receiver side...
		getsockname(ReceivingSocket, (SOCKADDR *)&ReceiverAddr, (int *)sizeof(ReceiverAddr));
	}

	void Setup(int Port)
	{
		BufLength = 1024;
		SenderAddrSize = sizeof(SenderAddr);

		// Initialize Winsock version 2.2
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			printf("Server: WSAStartup failed with error %ld\n", WSAGetLastError());
			printf("ERRRRRRRRORRRRRRRR\n\n\n\n\n");
		}

		// Create a new socket to receive datagrams on.
		ReceivingSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

		if (ReceivingSocket == INVALID_SOCKET)
		{
			printf("Server: Error at socket(): %ld\n", WSAGetLastError());
			// Clean up
			WSACleanup();
			// Exit with error
			printf("ERRRRRRRRORRRRRRRR\n\n\n\n\n");
		}

		// Set up a SOCKADDR_IN structure that will tell bind that we
		// want to receive datagrams from all interfaces using port 5150.

		// The IPv4 family
		ReceiverAddr.sin_family = AF_INET;
		// Port no. 5150
		ReceiverAddr.sin_port = htons(Port);
		// From all interface (0.0.0.0)
		ReceiverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

		// Associate the address information with the socket using bind.
		// At this point you can receive datagrams on your bound socket.
		if (bind(ReceivingSocket, (SOCKADDR *)&ReceiverAddr, sizeof(ReceiverAddr)) == SOCKET_ERROR)
		{
			printf("Server: bind() failed! Error: %ld.\n", WSAGetLastError());
			// Close the socket
			closesocket(ReceivingSocket);
			// Do the clean up
			WSACleanup();
			// and exit with error
			printf("ERRRRRRRRORRRRRRRR\n\n\n\n\n");
		}

		// Some info on the receiver side...
		getsockname(ReceivingSocket, (SOCKADDR *)&ReceiverAddr, (int *)sizeof(ReceiverAddr));
	}

	int Read(char* ReceiveBuf, int buffLen)
	{
		int bytesRecvd = 0;
		// Call recvfrom() to get it then display the received data...
		bytesRecvd = recvfrom(ReceivingSocket, ReceiveBuf, buffLen,
			0, (SOCKADDR *)&SenderAddr, &SenderAddrSize);
		if (bytesRecvd > 0)
		{
			;
		}
		else if (bytesRecvd <= 0)
			printf("Server: Connection closed with error code: %ld\n",
			WSAGetLastError());
		else
			printf("Server: recvfrom() failed with error code: %d\n",
			WSAGetLastError());

		/*char *RecvBuff = new char[bytesRecvd];
		
		for (int i = 0; i < bytesRecvd; i++)
			RecvBuff[i] = ReceiveBuf[i];

			*/

		// Some info on the sender side
		//getpeername(ReceivingSocket, (SOCKADDR *)&SenderAddr, &SenderAddrSize);
		//printf("Server: Sending IP used: %s\n", inet_ntoa(SenderAddr.sin_addr));
		//printf("Server: Sending port used: %d\n", htons(SenderAddr.sin_port));

		return bytesRecvd;
	}

	void Cleanup()
	{
		// When your application is finished receiving datagrams close the socket.
		printf("Server: Finished receiving. Closing the listening socket...\n");
		if (closesocket(ReceivingSocket) != 0)
			printf("Server: closesocket() failed! Error code: %ld\n", WSAGetLastError());
		else
			printf("Server: closesocket() is OK...\n");

		// When your application is finished call WSACleanup.
		printf("Server: Cleaning up...\n");
		if (WSACleanup() != 0)
			printf("Server: WSACleanup() failed! Error code: %ld\n", WSAGetLastError());
		else
			printf("Server: WSACleanup() is OK\n");
		// Back to the system
	}
private:
	WSADATA            wsaData;
	SOCKET             ReceivingSocket;
	SOCKADDR_IN        ReceiverAddr;
	int                BufLength;
	SOCKADDR_IN        SenderAddr;
	int                SenderAddrSize;
	int                ErrorCode;
};

#endif //__RecvUDP_H__