/**	\file Comm.h
Communications header file.
Contains the declarations of the Communications (Comm) Class.

Author: Emmanuel Harley
Date: 18/12/2014
*/
#ifndef COMM_H
#define COMM_H

//required for UDP to work
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <winsock2.h>
#include "RecvUDP.h"
#include "SendUDP.h"
#include "Serial.h"

/// Communication options
enum COMM{
	_SERIAL, _UDP
};

/** The communications class
	/para The Comm class allows the for UDP and Serial communications to
	be swapped easily in the code and mantain the same function calls for
	the communications
*/
class Comm
{
public:
	Comm()
	{}
	Comm(COMM type, int port, long baud = CBR_9600, string destIP = "");
	
	void Setup(COMM type, int port, long baud = CBR_9600, string destIP = "");
	void SetupUDP(unsigned short port = 0);
	void SetupSerial(int port, long baud);
	void Send(char* send, int byteCount);
	int Read(char* buffer, DWORD bytesToRead_buffSize = 0);

private:
	COMM comm;
	string arduinoIP;

	//UDP Instances
	UDPSocket *socket;
	WSASession *session;
	RecvUDP *hi;
	unsigned short Port;

	//Serial Instances
	Serial *serial;
};

#endif //__COMM_H__