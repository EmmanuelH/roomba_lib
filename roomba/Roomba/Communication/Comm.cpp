/**	\file Comm.cpp
Communications source file.
Contains the definitions of the Communications (Comm) Class.

Author: Emmanuel Harley
Date: 18/12/2014
*/
#include "Comm.h"


/** Constructor for Comm class with setup

	\param type The Communication type that is being used ex: _UDP
	\param port The port that should be used for either UDP or Serial
	\param baud The baud rate for the device, 9600 by default
	\param destIP The destination IP of device, "" by default
*/
Comm::Comm(COMM type, int port, long baud, string destIP)
{
	Setup(type, port, baud, destIP);
}

/** Sets up the communication line.

	\param type The Communication type that is being used ex: _UDP
	\param port The port that should be used for either UDP or Serial
	\param baud The baud rate for the device, 9600 by default
	\param destIP The destination IP of device, "" by default
*/
void Comm::Setup(COMM type, int port, long baud, string destIP)
{
	comm = type;
	arduinoIP = destIP;
	if (type == _SERIAL)
		SetupSerial(port, baud);
	else if (type == _UDP)
		SetupUDP(port);
}
/**	Initializes the UDP objects
	
	/param port Selected port to read from and write to
*/
void Comm::SetupUDP(unsigned short port)
{
	session = new WSASession;
	hi = new RecvUDP(port);
	socket = new UDPSocket;
	Port = port;
}

/** Initializes Serial objects
	
	\param port The com port the serial
*/
void Comm::SetupSerial(int port, long baud)
{
	serial = new Serial;
	serial->SetConfigure(baud, FALSE, 8, ONESTOPBIT);
}


/** Sends bytes to destination
	
	\param send Array of bytes to send to the device.
	\param byteCount The ammount of bytes to send to the device.
*/
void Comm::Send(char* send, int byteCount)
{
	int bytesWritten;
	//dunno if serial will work like this.
	if (comm == _SERIAL)
		DWORD error = serial->Write(send, byteCount, (DWORD &)bytesWritten);
	else if (comm == _UDP)
		socket->SendTo(arduinoIP, Port, send, byteCount);
}

/**	Reads bytes from buffer and returns bytes read
	
	\param buffer Contains the bytes that are read
	\param bytesToRead_buffSize The amount of bytes that should be read (serial only), The size of the buffer (udp only)
	\return Returns the amount of bytes read
*/
int Comm::Read(char *buffer, DWORD bytesToRead_buffSize)
{
	int bytesRead;
	int my = sizeof(buffer);
	//serial is untested
	if (comm == _SERIAL)
		DWORD error = serial->Read(buffer, bytesToRead_buffSize, (DWORD &)bytesRead);
	else if (comm == _UDP)
		bytesRead = hi->Read(buffer, bytesToRead_buffSize);
	
	return bytesRead;
}