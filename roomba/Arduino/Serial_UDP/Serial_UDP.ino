#include <SPI.h>         
#include <Ethernet.h>
#include <Wire.h>
#include <EthernetUdp.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// If using software SPI (the default case):
#define OLED_MOSI  9 //gry
#define OLED_CLK   8 //brn
#define OLED_DC    7 //blu
#define OLED_CS    6 //yel
#define OLED_RESET 32
Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);


IPAddress ip(192, 168, 1, 178);

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  
  0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xED };

unsigned int localPort = 7;      // local port to listen for UDP packets

const int BUFFER_SIZE = 80; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[BUFFER_SIZE]; //buffer to hold incoming and outgoing packets 

// A UDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

void setup() 
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  Serial1.begin(115200);
  // start Ethernet and UDP
  Ethernet.begin(mac, ip);
  Udp.begin(localPort);
  
  display.begin(SSD1306_SWITCHCAPVCC);
  display.display();
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(">>> clear to send <<<");
  display.display();
}

void loop()
{
  int packetSize = Udp.parsePacket();
  if (packetSize) {  
    // We've received a packet, read the data from it
    Udp.read(packetBuffer, BUFFER_SIZE);  // read the packet into the buffer
    
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    for(int i=0;i<packetSize;i++){
      display.println(packetBuffer[i]);
    }
    
    display.display();
    // send data to roomba
    Serial1.write(packetBuffer, packetSize);
  }

  String sendBuffer = "";
  while (Serial1.available() > 0) {
    // read the incoming byte:
    sendBuffer+= char(Serial1.read());
  }

  int buffSize = sendBuffer.length();
  if(buffSize){
    char *sendBuff = new char[buffSize];
    for(int i = 0; i < buffSize; i++){
      sendBuff[i] = sendBuffer[i];
    //Serial.println(sendBuff[i]);
    }
    Udp.beginPacket(Udp.remoteIP(), 7);
    Udp.write(sendBuff, buffSize);
    Udp.endPacket();

    delete[] sendBuff;
  }
}
